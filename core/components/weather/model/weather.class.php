<?php

class Weather {
	/** @var modX $modx */
	public $modx;
	/** @var array $config */
	public $config = array();


	/**
	 * @param modX $modx
	 * @param array $config
	 */
	function __construct(modX &$modx, array $config = array()) {
		$this->modx = $modx;
		$this->config = array_merge(
			array(
				'city' => 27612,
				'limit' => 0,
				'attempts' => 10,
				'mode' => 'basic',
				'url' => 'https://api.weather.yandex.ru/v1/forecast?geoid=[[+city]]&lang=ru',
                'timeout' => '3',
				'tplDetailed' => 'tpl.Weather.detailed',
				'tplDetailedPart' => 'tpl.Weather.detailed_part',
				'tplShort' => 'tpl.Weather.short',
				'tplBasic' => 'tpl.Weather.basic',

				'cacheTime' => 600,
				'imgDir' => MODX_ASSETS_PATH . 'components/weather/img/',
				'imgUrl' => MODX_ASSETS_URL . 'components/weather/img/',
				'registerCss' => '[[+assetsUrl]]components/weather/css/weather.css',

                'device_id' => $this->modx->getOption('weather_device_id', null, ''),
                'uuid' => $this->modx->getOption('weather_uuid', null, ''),
			),
			$config
		);
	}


	/**
	 *
	 */
	public function run() {
		if (!empty($this->config['registerCss'])) {
			$file = str_replace('[[+assetsUrl]]', MODX_ASSETS_URL, $this->config['registerCss']);
			$this->modx->regClientCSS(str_replace('//', '/', $file));
		}

		if (!$data = $this->getDataCache()) {
			return '';
		}

		$output = '';
		switch ($this->config['mode']) {
			case 'detailed':
				foreach ($data['forecasts'] as $idx => $day) {
					if ($this->config['limit'] && $idx >= $this->config['limit']) {
						break;
					}
					$output .= $this->getDay($day);
				}
				break;
			case 'short':
				foreach ($data['forecasts'] as $idx => $day) {
					if ($this->config['limit'] && $idx >= $this->config['limit']) {
						break;
					}
					$output .= $this->getDayShort($day);
				}
				break;
			default:
				$output = $this->getBasic($data);
		}

		return $output;
	}


	/**
	 * @param array $array
	 *
	 * @return bool|string
	 */
	public function getDay(array $array = array())                               // выдает массив
	{
		$parts = '';
		$sunrise = $array['sunrise'];                                               // вывод восхода
		$sunset = $array['sunset'];                                                 // вывод заката
		$day_week = $this::getDayRus(date('w', strtotime($array['date']))); // вывод дня недели
		$day_number = date('j', strtotime($array['date']));          // вывод числа
		$month = $this::getMonthRus(date('n', strtotime($array['date'])));  // вывод месяца


        $idx = 0;
		// последовательный перебор массива с данными по части дня
		foreach ($array['parts'] as $k => $part) {
            $idx++;
			// выкидываем из результатов массива середину дня и ночи
			if ($idx > 3) {
				break;
			}
			// вывод части дня в температуре
			if ($k == 'morning') {
				$part['part'] = 'Утром';
			}
			if ($k == 'day') {
				$part['part'] = 'Днем';
			}
			if ($k == 'evening') {
				$part['part'] = 'Вечером';
			}
			if ($k == 'night') {
				$part['part'] = 'Ночью';
			}

			// вывод поворота стрелочки направления ветра
			$part['wind_icon'] = $this->wind_arr($part['wind_dir']);
			$part['wind'] = $this->getImage('wind_icon');
			$part['wind_direction'] = $this::wind_dir($part['wind_dir']);

            //  добавление элементов массива для плейсхолдеров
            if (isset($part['pressure_mm'])) { $part['pressure'] = $part['pressure_mm']; }
            if (isset($part['temp_min'])) { $part['temperature_from'] = $part['temp_min']; }
            if (isset($part['temp_max'])) { $part['temperature_to'] = $part['temp_max']; }
            if (isset($part['temp_avg'])) { $part['temperature'] = $part['temp_avg']; }
			// вывод изменения цвета фона температуры в зависимости от значений оной
			@$temp_today = isset($part['temp_min'])
				? (($part['temp_min'] + $part['temp_max']) / 2)
				: $part['temp_avg'];
			$part['css_class'] = $this::getTempClass($temp_today);

			// вывод картинки сильного ветра в зависимости от скорости ветра
			if ($part['wind_speed'] > 7) {
				$part['wind_strength'] = $this->getImage('wind_strength');
			}
			else {
				$part['wind_strength'] = '';
			}

			// вывод знака "+" перед температурой
			foreach (array('temperature_from', 'temperature_to', 'temperature') as $v) {
				if (isset($part[$v])) {
					$part[$v] = $this::plus($part[$v]);
				}
				else {
					$part[$v] = '';
				}
			}
			// получение картинок
			$part['image'] = $this->getImage($part['icon']);
			// постановка условия: какой шаблон выводить в зависимости от температуры
			$parts .= $this->getChunk('tplDetailedPart', $part);
		}
		// разные значения внутри массива
		$placeholders = array(
			'parts' => $parts,
			'date' => $array['date'],
			'sunrise' => $sunrise,
			'sunset' => $sunset,
			'day_week' => $day_week,
			'day_number' => $day_number,
			'month' => $month,
		);

		// вывод выходных дней в году другим стилем
		$placeholders['css_class'] = $this::isHoliday($array['date'])
			? 'holiday'
			: 'workday';

		return $this->getChunk('tplDetailed', $placeholders);
	}


	/**
	 * @param array $array
	 *
	 * @return bool|string
	 */
	public function getDayShort(array $array = array()) {
		$day_week = $this::getDayRus(date('w', strtotime($array['date'])));  // вывод дня недели
		$day_number = date('j', strtotime($array['date']));           // вывод числа
		$month = $this::getMonthRus(date('n', strtotime($array['date'])));   // вывод месяца

		$placeholders = array(
			'date' => $array['date'],
			'day_week' => $day_week,
			'day_number' => $day_number,
			'month' => $month,
		);
		// День
		$day = $array['parts']['day_short'];
		$placeholders['image'] = $this->getImage($day['icon']);
		$placeholders['weather_type'] = $this->getConditionRus($day['condition']);
		$temperature_day = $day['temp'];
		$placeholders['css_class_day'] = $this::getTempClass($temperature_day);
		$placeholders['temperature_day'] = $this::plus($temperature_day);
		// Ночь
		$night = $array['parts']['night_short'];
		$temperature_night = $night['temp'];
		$placeholders['css_class_night'] = $this::getTempClass($temperature_night);
		$placeholders['temperature_night'] = $this::plus($temperature_night);

		// вывод выходных дней в году другим стилем
		$placeholders['css_class_date'] = $this::isHoliday($array['date'])
			? 'holiday'
			: 'workday';

		return $this->getChunk('tplShort', $placeholders);
	}


	/**
	 * @param $array
	 *
	 * @return bool|string
	 */
	public function getBasic($array) {
		$placeholders = array(                                           // прописываем плейсхолдеры из шаблона tpl outer
			'id' => $array['geo_object']['locality']['id'],
			'city' => $array['geo_object']['locality']['name'],                    // город
			'date' => date('H:i:s', strtotime($array['now'])), // настоящее время

			'temp' => $this::plus($array['fact']['temp']),              // температура со знаком "+" настоящего времени
			'hour1' => $this::get_sting_from_hour(date('G', strtotime($array['now']))),                  // вывод времени суток №1
			'hour2' => $this::get_sting_from_hour1(date('G', strtotime($array['now']))),                 // вывод времени суток №2
			'temp1' => $this::temp1($array),                                   // вывод температуры для времени суток №1
			'temp2' => $this::temp2($array),                                   // вывод температуры для времени суток №2
			'weather_type' => $this->getConditionRus($array['fact']['condition']),           // вывод типа погоды настоящего времени

			'sunrise' => $array['forecasts'][0]['sunrise'],                   // вывод времени восхода текущего дня
			'sunset' => $array['forecasts'][0]['sunset'],                     // вывод времени заката текущего дня
			'wind_speed' => $array['fact']['wind_speed'],               // вывод скорости ветра настоящего времени
			'wind_dir' => $this::wind_dir($array['fact']['wind_dir']),   // вывод направления ветра на русском настоящего времени
			'wind_icon' => $this::wind_arr($array['fact']['wind_dir']),
			'humidity' => $array['fact']['humidity'],                   // вывод влажности настоящего времени
			'pressure' => $array['fact']['pressure_mm'],                   // вывод давления настоящего времени
			'temp_y' => 'n/a',                                          // устарело //$this::plus($array['yesterday']['temperature']),       // вывод температуры вчерашнего дня
			'image_1' => $this->getImage($array['fact']['icon']),                   // вывод картинки погоды настоящего времени
			'image_2' => $this->getImage($this::get_image_from_hour($array)),                   // вывод картинки погоды для времени суток №1
			'image_3' => $this->getImage($this::get_image_from_hour1($array)),                  // вывод картинки погоды для времени суток №2
			'observation_time' => date('H:i', strtotime($array['fact']['obs_time'])),  // вывод времени обновления данных
			'water_temperature' => $this::tempWater($array),
			'water' => $this::tempWaterClass($array),

			'css_class_1' => $this::getTempClass($array['fact']['temp']),
			'css_class_2' => $this::getTempClass($this::temp1($array)),
			'css_class_3' => $this::getTempClass($this::temp2($array)),
		);

		return $this->getChunk('tplBasic', $placeholders);
	}


	/**
	 * @param string $name Имя чанка
	 * @param array $properties
	 *
	 * @return bool|string
	 */
	public function getChunk($name, array $properties = array()) {

		$properties['img_url'] = $this->config['imgUrl'];
		$chunk = $this->config[$name];

		if (class_exists('pdoTools') && $pdo = $this->modx->getService('pdoTools')) {
			return $pdo->getChunk($chunk, $properties);
		}
		else {
			return $this->modx->getChunk($chunk, $properties);
		}
	}


	/**
	 * @return array|bool
	 */
	public function getDataCache() {
		/** @var xPDOCacheManager $cacheManager */
		$cacheManager = $this->modx->getCacheManager();
		$cache_key = 'weather/' . $this->config['city'];

		$json = $cacheManager->get($cache_key);
		if (empty($json)) {
			$url = str_replace('[[+city]]', trim($this->config['city']), $this->config['url']);
			$attempts = (int)$this->config['attempts']
				? (int)$this->config['attempts']
				: 10;
			for ($i = 1; $i <= $attempts; $i++) {
				if ($data = $this->download($url, array('isData' => true))) {
					break;
				}
			}
			if (!empty($data)) {
                $json = $data;
				$cacheManager->set($cache_key, $json, $this->config['cacheTime']);
			}
			else {
				$this->modx->log(modX::LOG_LEVEL_ERROR,
					"[Weather] Could not download data from: {$url} after {$attempts} attempts"
				);
			}
		}

		return json_decode($json, true);
	}


	/**
	 * @param $temp
	 *
	 * @return string
	 */
	public static function plus($temp) {
		if ($temp > 0) {
			$temp = '+' . $temp;
		}

		return $temp;
	}


	/**
	 * функция вывода на русском названия времени суток №1
	 *
	 * @param $date
	 *
	 * @return string
	 */
	public static function get_sting_from_hour($date) {
		if ($date <= 12 and $date > 6) {
			$dir = 'Днем';
		}
		elseif ($date <= 18 and $date > 12) {
			$dir = "Вечером";
		}
		elseif ($date <= 24 and $date > 18) {
			$dir = "Ночью";
		}
		else {
			$dir = "Утром";
		}
		return $dir;
	}


	/**
	 * функция вывода на русском названия времени суток №2
	 *
	 * @param $date
	 *
	 * @return string
	 */
	public static function get_sting_from_hour1($date) {
		if ($date <= 12 and $date > 6) {
			$dir = 'Вечером';
		}
		elseif ($date <= 18 and $date > 12) {
			$dir = "Ночью";
		}
		elseif ($date <= 24 and $date > 18) {
			$dir = "Утром";
		}
		else {
			$dir = "Днем";
		}
		return $dir;
	}


	/**
	 * функция вывода температуры в зависимости от времени суток №1
	 *
	 * @param $array
	 *
	 * @return float|string
	 */
	protected static function temp1($array) {
		$date = date('G', strtotime($array['now']));

		if ($date <= 12 and $date > 6) {
			$temp = isset($array['forecasts'][0]['parts']['morning']['temperature_min'])
				? ceil(($array['forecasts'][0]['parts']['morning']['temperature_min'] + $array['forecasts'][0]['parts']['morning']['temperature_max']) / 2)
				: $array['forecasts'][0]['parts']['morning']['temp_avg'];
			if ($temp > 0) {
				$temp = '+' . $temp;
			}
		}
		elseif ($date <= 18 and $date > 12) {
			$temp = isset($array['forecasts'][0]['parts']['day']['temperature_min'])
				? ceil(($array['forecasts'][0]['parts']['day']['temperature_min'] + $array['forecasts'][0]['parts']['day']['temperature_max']) / 2)
				: $array['forecasts'][0]['parts']['day']['temp_avg'];
			if ($temp > 0) {
				$temp = '+' . $temp;
			}
		}
		elseif ($date <= 24 and $date > 18) {
			$temp = isset($array['forecasts'][0]['parts']['evening']['temperature_min'])
				? ceil(($array['forecasts'][0]['parts']['evening']['temperature_min'] + $array['forecasts'][0]['parts']['evening']['temperature_max']) / 2)
				: $array['forecasts'][0]['parts']['evening']['temp_avg'];
			if ($temp > 0) {
				$temp = '+' . $temp;
			}
		}
		else {
			$temp = isset($array['forecasts'][0]['parts']['night']['temperature_min'])
				? ceil(($array['forecasts'][0]['parts']['night']['temperature_min'] + $array['forecasts'][0]['parts']['night']['temperature_max']) / 2)
				: $array['forecasts'][0]['parts']['night']['temp_avg'];
			if ($temp > 0) {
				$temp = '+' . $temp;
			}
		}

		return $temp;
	}


	/**
	 * функция вывода температуры в зависимости от времени суток №2
	 *
	 * @param $array
	 *
	 * @return float|string
	 */
	protected static function temp2($array) {
		$date = date('G', strtotime($array['now']));
		if ($date <= 12 and $date > 6) {
			$temp = isset($array['forecasts'][0]['parts']['day']['temperature_min'])
				? ceil(($array['forecasts'][0]['parts']['day']['temperature_min'] + $array['forecasts'][0]['parts']['day']['temperature_max']) / 2)
				: $array['forecasts'][0]['parts']['day']['temp_avg'];
			if ($temp > 0) {
				$temp = '+' . $temp;
			}
		}
		elseif ($date <= 18 and $date > 12) {
			$temp = isset($array['forecasts'][0]['parts']['evening']['temperature_min'])
				? ceil(($array['forecasts'][0]['parts']['evening']['temperature_min'] + $array['forecasts'][0]['parts']['evening']['temperature_max']) / 2)
				: $array['forecasts'][0]['parts']['evening']['temp_avg'];
			if ($temp > 0) {
				$temp = '+' . $temp;
			}
		}
		elseif ($date <= 24 and $date > 18) {
			$temp = isset($array['forecasts'][1]['parts']['night']['temperature_min'])
				? ceil(($array['forecasts'][1]['parts']['night']['temperature_min'] + $array['forecasts'][1]['parts']['night']['temperature_max']) / 2)
				: $array['forecasts'][1]['parts']['night']['temp_avg'];
			if ($temp > 0) {
				$temp = '+' . $temp;
			}
		}
		else {
			$temp = isset($array['forecasts'][0]['parts']['morning']['temperature_min'])
				? ceil(($array['forecasts'][0]['parts']['morning']['temperature_min'] + $array['forecasts'][0]['parts']['morning']['temperature_max']) / 2)
				: $array['forecasts'][0]['parts']['morning']['temp_avg'];
			if ($temp > 0) {
				$temp = '+' . $temp;
			}
		}

		return $temp;
	}


	/**
	 * функция вывода направления ветра на русском
	 *
	 * @param $wind
	 *
	 * @return string
	 */
	protected static function wind_dir($wind) {
		switch ($wind) {
			case 'n':
				$dir = 'с';
				break;
			case 'w':
				$dir = 'з';
				break;
			case 'e':
				$dir = 'в';
				break;
			case 's':
				$dir = 'ю';
				break;
			case 'ne':
				$dir = 'св';
				break;
			case 'se':
				$dir = 'юв';
				break;
			case 'nw':
				$dir = 'сз';
				break;
			case 'sw':
				$dir = 'юз';
				break;
			case 'calm':
				$dir = 'Штиль';
				break;
			default:
				$dir = 'непонятное';
		}

		return $dir;
	}


	/**
	 * Вывод поворота стрелочки направления ветра
	 *
	 * @param $wind
	 *
	 * @return string
	 */
	protected static function wind_arr($wind) {
		switch ($wind) {
			case 'n':
				$arr = 'wind_180';
				break;
			case 'w':
				$arr = 'wind_90';
				break;
			case 'e':
				$arr = 'wind_270';
				break;
			case 's':
				$arr = 'wind_0';
				break;
			case 'ne':
				$arr = 'wind_225';
				break;
			case 'se':
				$arr = 'wind_315';
				break;
			case 'nw':
				$arr = 'wind_135';
				break;
			case 'sw':
				$arr = 'wind_45';
				break;
			default:
				$arr = 'wind_calm';
		}

		return $arr;
	}


	/**
	 * функция вывода картинки погоды для времени суток №1
	 *
	 * @param $array
	 *
	 * @return mixed
	 */
	protected static function get_image_from_hour($array) {
		$date = date('G', strtotime($array['now']));
		if ($date <= 12 and $date > 6) {
			$dir = $array['forecasts'][0]['parts']['morning']['icon'];
		}
		elseif ($date <= 18 and $date > 12) {
			$dir = $array['forecasts'][0]['parts']['day']['icon'];
		}
		elseif ($date <= 24 and $date > 18) {
			$dir = $array['forecasts'][0]['parts']['evening']['icon'];
		}
		else {
			$dir = $array['forecasts'][0]['parts']['night']['icon'];
		}

		return $dir;
	}


	/**
	 * функция вывода картинки погоды для времени суток №2
	 *
	 * @param $array
	 *
	 * @return mixed
	 */
	protected static function get_image_from_hour1($array) {
		$date = date('G', strtotime($array['now']));
		if ($date <= 12 and $date > 6) {
			$dir = $array['forecasts'][0]['parts']['day']['icon'];
		}
		elseif ($date <= 18 and $date > 12) {
			$dir = $array['forecasts'][0]['parts']['evening']['icon'];
		}
		elseif ($date <= 24 and $date > 18) {
			$dir = $array['forecasts'][1]['parts']['night']['icon'];
		}
		else {
			$dir = $array['forecasts'][0]['parts']['morning']['icon'];
		}

		return $dir;
	}


	/**
	 * Функция вывода названия дня недели на русском
	 *
	 * @param bool $num_day
	 *
	 * @return mixed
	 */
	protected static function getDayRus($num_day = false) {
		if ($num_day >= 7) {
			$num_day = date('w');
		}
		$days = array(
			'вс', 'пн',
			'вт', 'ср',
			'чт', 'пт', 'сб'
		);
		$name_day = $days[$num_day];

		return $name_day;
	}


	/**
	 * Функция вывода названия месяца на русском
	 *
	 * @param bool $num_month
	 *
	 * @return mixed
	 */
	protected static function getMonthRus($num_month = false) {
		if (!$num_month) {
			$num_month = date('n');
		}
		$monthes = array(
			1 => 'января', 2 => 'февраля', 3 => 'марта',
			4 => 'апреля', 5 => 'мая', 6 => 'июня',
			7 => 'июля', 8 => 'августа', 9 => 'сентября',
			10 => 'октября', 11 => 'ноября',
			12 => 'декабря'
		);
		$name_month = $monthes[$num_month];

		return $name_month;
	}


	/**
	 * @param $image
	 * @param string $ext
	 *
	 * @return bool|string
	 */
	public function getImage($image, $ext = 'svg') {
		$image .= '.' . $ext;
		$cache = $this->config['imgDir'] . $image;

		if (!file_exists($cache)) {
			if ($tmp = $this->download("https://yastatic.net/weather/i/icons/blueye/color/svg/{$image}")) {
				file_put_contents($cache, $tmp);
			}
			else {
				return false;
			}
		}

		return $this->config['imgUrl'] . $image;
	}


	/**
	 * @param $temp
	 *
	 * @return string
	 */
	public static function getTempClass($temp) {
		if ($temp > 60) {
			$class = 'temphot';
		}
		elseif ($temp < -60) {
			$class = 'tempcold';
		}
		else {
			$class = 'temp' . (int)$temp;
		}

		return $class;
	}


	/**
	 * @param $array
	 *
	 * @return bool|string
	 */
	protected function tempWater($array) {

		$tempWater = isset($array['fact']['water_temperature']);

		if ($tempWater) {
			$Water = 'Температура воды: +' . $array['fact']['water_temperature'] . '°С';
		}
		else $Water = False;
		return $Water;
	}


	/**
	 * @param $array
	 *
	 * @return string
	 */
	protected function tempWaterClass($array) {
		$tempWater = isset($array['fact']['water_temperature']);

		if ($tempWater) {
			$Water = 'water';
		}
		else $Water = 'water_no';
		return $Water;
	}


	/**
	 * @param $date
	 *
	 * @return bool
	 */
	protected function isHoliday($date) {
		if (!is_numeric($date)) {
			$date = strtotime($date);
		}

		$dow = date('w', $date);
		if ($dow == 0 || $dow == 6) {
			return true;
		}

		$month = date('m', $date);
		$day = date('d', $date);
		if ($month == 1 && $day >= 1 && $day <= 10) {
			return true;
		}

		$holidays = array(
			'23.02', '08.03', '01.05', '02.05', '03.05', '04.05',
			'09.05', '10.05', '11.05', '12.06', '04.11'
		);

		return in_array($day . '.' . $month, $holidays);
	}


    /**
     * Функция вывода типа погоды на русском
     *
     * @param string $condition
     * @return string
     */
    protected function getConditionRus($condition = '') {
        switch($condition) {
            case 'overcast':
                $condition = 'Пасмурно';
                break;
            case 'clear':
                $condition = 'Ясно';
                break;
            case 'cloudy':
                $condition = 'Облачность';
                break;
            case 'partly-cloudy':
                $condition = 'Частичная облачность';
                break;
            case 'cloudy-and-light-rain':
                $condition = 'Облачность и небольшой дождь';
                break;
            case 'cloudy-and-rain':
                $condition = 'Облачность и дождь';
                break;
            case 'overcast-and-light-rain':
                $condition = 'Пасмурно и небольшой дождь';
                break;
            case 'partly-cloudy-and-light-rain':
                $condition = 'Частичная облачность и небольшой дождь';
                break;
            case 'partly-cloudy-and-rain':
                $condition = 'Частичная облачность и дождь';
                break;
            case 'overcast-and-rain':
                $condition = 'Пасмурно и дождь';
                break;
            case 'overcast-thunderstorms-with-rain':
                $condition = 'Пасмурно и дождь с грозой';
                break;
            case 'overcast-and-wet-snow':
                $condition = 'Пасмурно и мокрый снег';
                break;
            case 'overcast-and-light-snow':
                $condition = 'Пасмурно и небольшой снег';
                break;

        }

        return $condition;
    }


	/**
	 * @param $src
	 * @param array $options
	 *
	 * @return bool|mixed|string
	 */
	protected function download($src, $options = array()) {
		if (function_exists('curl_init')) {
            $options = array_merge(
                array('timeout' => $this->config['timeout']),
                $options
            );
			$ch = curl_init();
//			$fOut = fopen($_SERVER["DOCUMENT_ROOT"].'/'.'curl_out.txt', "w" );
//			curl_setopt($ch, CURLOPT_VERBOSE, 1);
//			curl_setopt($ch, CURLOPT_STDERR, $fOut );
			curl_setopt($ch, CURLOPT_URL, $src);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, $options['timeout']);

            if (isset($options['isData']) and $options['isData']) {
                $secret = 'eternalsun';
                $device_id = $this->config['device_id'];
                $uuid = $this->config['uuid'];
                $timestamp = time();
                $token = md5($secret.$timestamp);
                $request = 'User-Agent: yandex-weather-android/4.2.1
X-Yandex-Weather-Client: YandexWeatherAndroid/4.2.1
X-Yandex-Weather-Device: os=null;os_version=21;manufacturer=chromium;model=App Runtime for Chrome Dev;device_id=' . $device_id . ';uuid=' . $uuid . ';
X-Yandex-Weather-Token: ' . $token . '
X-Yandex-Weather-Timestamp: ' . $timestamp . '
X-Yandex-Weather-UUID: ' . $uuid . '
X-Yandex-Weather-Device-ID: ' . $device_id . '
Host: api.weather.yandex.ru
Connection: Keep-Alive';
                $split = explode("\n", $request);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $split);
            }

			$safeMode = @ini_get('safe_mode');
			$openBasedir = @ini_get('open_basedir');
			if (empty($safeMode) && empty($openBasedir)) {
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			}
			$file = curl_exec($ch);
			$info = curl_getinfo($ch);
			if ($info['http_code'] != 200) {
				//$this->modx->log(modX::LOG_LEVEL_ERROR, '[Weather] Could not download data from: ' . $src);
				$file = '';
			}
			curl_close($ch);
		}
		else {
			$file = @file_get_contents($src);
		}

		return $file;
	}

}