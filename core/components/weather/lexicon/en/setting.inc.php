<?php

$_lang['area_weather_main'] = 'Основные';

$_lang['setting_weather_device_id'] = 'Device ID';
$_lang['setting_weather_device_id_desc'] = 'Yandex requires transfer device ID. Generates automatically by installer, but you can type own value';

$_lang['setting_weather_uuid'] = 'UUID';
$_lang['setting_weather_uuid_desc'] = 'Yandex requires transfer UUID. Generates automatically by installer, but you can type own value';