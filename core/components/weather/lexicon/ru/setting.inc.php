<?php

$_lang['area_weather_main'] = 'Основные';

$_lang['setting_weather_device_id'] = 'ID устройства';
$_lang['setting_weather_device_id_desc'] = 'Яндекс требует передачу ID устройства. При установке компонента генерируется автоматически, либо вы можете задать собственный';

$_lang['setting_weather_uuid'] = 'UUID';
$_lang['setting_weather_uuid_desc'] = 'Яндекс требует передачу UUID. При установке компонента генерируется автоматически, либо вы можете задать собственный';