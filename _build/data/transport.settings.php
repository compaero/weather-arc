<?php

$settings = array();

$tmp = array(
	'device_id' => array(
		'xtype' => 'textfield',
		'value' => true,
		'area' => 'weather_main',
	),
	'uuid' => array(
		'xtype' => 'textfield',
		'value' => true,
		'area' => 'weather_main',
	),

);

foreach ($tmp as $k => $v) {
	/* @var modSystemSetting $setting */
	$setting = $modx->newObject('modSystemSetting');
	$setting->fromArray(array_merge(
		array(
			'key' => 'weather_' . $k,
			'namespace' => PKG_NAME_LOWER,
		), $v
	), '', true, true);

	$settings[] = $setting;
}

unset($tmp);
return $settings;
